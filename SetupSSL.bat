CLS
@ECHO OFF
REM You need only run this once per implementation
REM if you feel you need to re-run it, you should run Cleanup.bat first.
REM		Registers certificates with the IP Ports used for the services
REM
Call EnvVars.bat
@ECHO.
@ECHO Registering Certificates with the service ports
@ECHO.
netsh http add sslcert ipport=%ServicePort% appid=%ServiceGuid% certhash=%ServiceCertThumbPrint%
@ECHO.
@ECHO %CName%
@ECHO.
netsh http add sslcert ipport=%STSPort% appid=%STSGuid% certhash=%STSCertThumbPrint%
::
netsh http show sslcert %ServicePort%
netsh http show sslcert %STSPort%
::
@ECHO.
@ECHO Finished
