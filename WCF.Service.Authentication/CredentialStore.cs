﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Security;
using Microsoft.IdentityModel.Claims;

namespace WCF.Service.Authentication
{
    public static class CredentialStore
    {
        private static Dictionary<string, string> Credentials { get; set; }

        static CredentialStore()
        {
            Credentials = new Dictionary<string, string> {{"Admin", "password"}, {"Guest", "password"}};
        }

        public static void AuthenticateUser(string username, string password)
        {
            string pw;
            if (!Credentials.TryGetValue(username, out pw) || !pw.Equals(password)) FailAuthentication();


            Trace.WriteLine(String.Format("Authenticated {0}", username));
        }

        private static void FailAuthentication()
        {
            throw new SecurityException("Access is denied. Invalid Username or Password.");
        }

        public static List<Claim> GetClaimsForUser(string username)
        {
            var claims = new List<Claim>();


            var nameClaim = new Claim(ClaimTypes.Name, username);
            claims.Add(nameClaim);


            var roleClaim = new Claim(ClaimTypes.Role, username == "Admin" ? "Admin" : "User");
            claims.Add(roleClaim);

            var emailClaim = new Claim(ClaimTypes.Email, string.Format("{0}@example.org", username));
            claims.Add(emailClaim);

            var userdataClaim = new Claim(ClaimTypes.UserData, Guid.NewGuid().ToString());
            claims.Add(userdataClaim);


            return claims;
        }
    }
}