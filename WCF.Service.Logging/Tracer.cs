﻿using System;
using System.Diagnostics;
using System.Reflection;

namespace WCF.Service.Logging
{
    public interface ITracer
    {
        void Trace(string message);
    }

    public class Tracer : ITracer
    {
        public void Trace(string message)
        {
            var method = new StackTrace().GetFrame(1).GetMethod();
            var typeName = method.DeclaringType != null ? string.Format("{0}.", method.DeclaringType.Name) : "";
            var methodName = method.Name;
            System.Diagnostics.Trace.WriteLine(
                String.Format("{0}\t{1}{2} {3}",
                              DateTime.Now.ToString("yyyy-mm-dd hh:mm:ss.ffffff"),
                              typeName,
                              methodName,
                              message
                    )
                );
        }
    }
}