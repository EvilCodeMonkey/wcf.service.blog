﻿using System.ServiceModel;
using WCF.Service.Contract;

namespace WCF.Service.Test
{
    public class ServiceClientAsync : ClientBase<IServiceAsync>
    {
        public ServiceClientAsync(string configurationName) :base(configurationName)
        {
            
        }
    }
}