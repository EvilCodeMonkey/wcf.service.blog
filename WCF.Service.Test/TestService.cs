using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.ServiceModel.Security;
using System.Threading;
using Microsoft.IdentityModel.Protocols.WSTrust;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Practices.Unity;
using Moq;
using NUnit.Framework;
using Unity.Wcf;
using WCF.Service.Contract;
using WCF.Service.Logging;
using WIF.STS.Library;

namespace WCF.Service.Test
{
    [TestFixture]
    public class TestService
    {
        private readonly WSTrustServiceHost _hostSTS;
        private readonly UnityServiceHost _host;
        private ServiceClient _client;
        private ServiceClientAsync _asyncClient;
        private IService _channel;
        private IServiceAsync _asyncChannel;
        public TestService()
        {
            ServicePointManager.ServerCertificateValidationCallback += 
                (s, c, chain, errors) =>
                {
                    var certificate = (X509Certificate2) c;
                    var result = (certificate.Subject ==
                                    ConfigurationManager.
                                        AppSettings[
                                            "SigningCertificateName"
                                        ])
                                ||
                                (certificate.Subject ==
                                    ConfigurationManager.
                                        AppSettings[
                                            "EncryptingCertificateName"
                                        ]);
                    return result;
                };
            _hostSTS = new WSTrustServiceHost(new CustomSecurityTokenServiceConfiguration());
            StartHost(_hostSTS, "WCF.STS.Host", "STS host");
            var container = new UnityContainer();
            container.RegisterType<IService, Library.Service>();
            container.RegisterType<ITracer, Tracer>();
            _host = new UnityServiceHost(container, container.Resolve<IService>().GetType());
            StartHost(_host, "WCF.Service.Host", "Service host");
        }

        private static void StartHost(ServiceHostBase host, string wifConfigurationName, string serviceTag)
        {
            FederatedServiceCredentials.ConfigureServiceHost(host, wifConfigurationName);
            host.Faulted +=
                (s, e) => Trace.WriteLine(string.Format("{0}  has faulted. Please restart the {0}.", serviceTag));
            host.Open();
            foreach (var baseAddress in host.BaseAddresses)
            {
                Trace.WriteLine(string.Format("{0} listening on: {1}", serviceTag, baseAddress.AbsoluteUri));
            }
        }

        ~TestService()
        {
            _host.Close();
            _hostSTS.Close();
        }

        [SetUp]
        public void Setup()
        {
            _client = new ServiceClient( "WS2007FederationHttpBinding_IService" );
            _asyncClient = new ServiceClientAsync( "WS2007FederationHttpBinding_IServiceAsync" );
        }

        [TearDown]
        public void Teardown()
        {
            if (_channel != null)
            {
                if (((IClientChannel) _channel).State == CommunicationState.Opened)
                {
                    ((IClientChannel) _channel).Close();
                }
                else
                {
                    ((IClientChannel) _channel).Abort();
                }
            }
            if (_asyncChannel != null)
            {
                if (((IClientChannel) _asyncChannel).State == CommunicationState.Opened)
                {
                    ((IClientChannel) _asyncChannel).Close();
                }
                else
                {
                    ((IClientChannel) _asyncChannel).Abort();
                }
            }
        }

        // This is just for async thread control
        private readonly SortedList<Guid, bool> _asyncCallbacksActive = new SortedList<Guid, bool>();

        private void WaitForCallBack(Guid id)
        {
            var startTime = DateTime.Now;
            while (_asyncCallbacksActive[id])
            {
                Thread.Sleep(10);
                if (startTime.Add(new TimeSpan(0, 0, 0, 10)).CompareTo(DateTime.Now) <= 0)
                {
                    // timeout exceeded
                    Assert.Fail();
                    break;
                }
            }
            _asyncCallbacksActive.Remove(id);
        }

        private void PrepareClient(string username, string password)
        {
            _client.ClientCredentials.UserName.UserName = username;
            _client.ClientCredentials.UserName.Password=password;
            _channel = _client.ChannelFactory.CreateChannel();

        }

        private void PrepareAsyncClient(string username, string password)
        {
            _asyncClient.ClientCredentials.UserName.UserName=username;
            _asyncClient.ClientCredentials.UserName.Password=password;
            _asyncChannel = _asyncClient.ChannelFactory.CreateChannel();
        }

        [Test]
        public void TestGetDataAsync()
        {
            PrepareAsyncClient("Guest", "password");
            const int value = 1;
            var id = Guid.NewGuid();
            _asyncCallbacksActive.Add(id, true);
            {
                _asyncChannel.BeginGetData(value,
                                     result =>
                                         {
                                             var newValue = _asyncChannel.EndGetData(result);
                                             Assert.AreNotEqual(string.Empty, newValue);
                                             _asyncCallbacksActive[id] = false;
                                         },
                                     null);
            }

            WaitForCallBack(id);
        }

        [Test]
        public void TestGetData()
        {
            PrepareClient("Guest", "password");
            var result = _channel.GetData(1);
            Assert.AreNotEqual(string.Empty, result);
        }


        [Test]
        public void TestGetLibrary()
        {
            PrepareClient("Admin", "password");
            var result = _channel.GetLibrary();
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Catalog);
            Assert.AreEqual(2, result.Catalog.Count);
        }

        [Test]
        public void TestGetLibraryRoleAccessDenied()
        {
            PrepareClient("Guest", "password");
            Assert.Throws<SecurityAccessDeniedException>(() => _channel.GetLibrary());
        }

        [Test]
        public void TestGetLibraryAccessDenied()
        {
            PrepareClient("Hacker", "password");
            Assert.Throws<MessageSecurityException>(() => _channel.GetLibrary());
        }

        [Test]
        public void TestGetLibraryAsync()
        {
            PrepareAsyncClient("Admin", "password");
            var id = Guid.NewGuid();
            _asyncCallbacksActive.Add(id, true);
            {
                _asyncChannel.BeginGetLibrary(
                    result =>
                        {
                            var newValue = _asyncChannel.EndGetLibrary(result);
                            Assert.IsNotNull(newValue);
                            Assert.IsNotNull(newValue.Catalog);
                            Assert.AreEqual(2, newValue.Catalog.Count);
                            _asyncCallbacksActive[id] = false;
                        },
                    null);
            }

            WaitForCallBack(id);
        }
    }
}