using System;
using System.Collections;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace WCF.Service.Contract
{
    /// <summary>
    /// We do not have to implement this interface in concrete code anywhere to get it to work.
    /// </summary>
    [ServiceContract(Name = "IService")]
    public interface IServiceAsync
    {

        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginGetData(int value, AsyncCallback callback, object state);
        string EndGetData(IAsyncResult result);
        
        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginGetLibrary(AsyncCallback callback, object state);
        LibraryCatalog EndGetLibrary(IAsyncResult result);
    }


    [ServiceContract]
    public interface IService
    {
        [OperationContract]
        string GetData(int i);

        [OperationContract]
        LibraryCatalog GetLibrary();
    }

    [DataContract]
    [KnownType(typeof (Book))]
    [KnownType(typeof (Magazine))]
    public class LibraryCatalog
    {
        [DataMember]
        public Hashtable Catalog { get; set; }
    }


    [DataContract]
    public class Book
    {
    }

    [DataContract]
    public class Magazine
    {
    }
}