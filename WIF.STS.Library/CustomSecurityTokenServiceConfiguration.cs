﻿
using System.Configuration;
using System.Security.Cryptography.X509Certificates;
using Microsoft.IdentityModel.Configuration;
using Microsoft.IdentityModel.SecurityTokenService;

namespace WIF.STS.Library
{
    /// <summary>
    ///   A custom SecurityTokenServiceConfiguration implementation.
    /// </summary>
    public class CustomSecurityTokenServiceConfiguration : SecurityTokenServiceConfiguration
    {
        /// <summary>
        ///   CustomSecurityTokenServiceConfiguration constructor.
        /// </summary>
        public CustomSecurityTokenServiceConfiguration()
            : base(ConfigurationManager.AppSettings[Common.IssuerName],
                   new X509SigningCredentials(CertificateUtil.GetCertificate(
                       StoreName.My, StoreLocation.LocalMachine,
                       ConfigurationManager.AppSettings[Common.SigningCertificateName])))
        {
            SecurityTokenService = typeof (CustomSecurityTokenService);
        }
    }
}