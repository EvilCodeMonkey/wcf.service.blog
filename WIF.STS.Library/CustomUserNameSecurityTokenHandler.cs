﻿using System.IdentityModel.Tokens;
using System.Security;
using Microsoft.IdentityModel.Claims;
using Microsoft.IdentityModel.Tokens;
using ClaimTypes = System.IdentityModel.Claims.ClaimTypes;

namespace WIF.STS.Library
{
    public class CustomUserNameSecurityTokenHandler : UserNameSecurityTokenHandler
    {
        public override bool CanValidateToken
        {
            get { return true; }
        }

        public override ClaimsIdentityCollection ValidateToken(SecurityToken token)
        {
            var userNameToken = token as UserNameSecurityToken;
            if (userNameToken == null)
                throw new SecurityException(
                    string.Format("Invalid token provided: {0}. Expecting UserNameSecurityToken.", token.GetType()));

            // TODO: authenticate the caller against the credential store
            // this sample uses an in-memory credential store
            // see CredentialStore.cs
            WCF.Service.Authentication.CredentialStore.AuthenticateUser(userNameToken.UserName, userNameToken.Password);
            // will throw if fails

            var identities = new ClaimsIdentityCollection();
            var claimsIdentity = new ClaimsIdentity("CustomUserNameSecurityTokenHandler");
            //claimsIdentity.Claims.AddRange(CredentialStore.GetClaimsForUser(userNameToken.UserName));

            claimsIdentity.Claims.Add(new Claim(ClaimTypes.Name, userNameToken.UserName));

            identities.Add(claimsIdentity);

            return identities;
        }
    }
}