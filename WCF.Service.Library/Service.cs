using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Threading;
using Microsoft.IdentityModel.Claims;
using WCF.Service.Contract;
using WCF.Service.Logging;

namespace WCF.Service.Library
{
    public class Service : IService
    {
        private readonly ITracer _tracer;

        public Service(ITracer tracer)
        {
            _tracer = tracer;
        }

        public string GetData(int value)
        {
            _tracer.Trace("Starting");
            CheckClaims(new[] {"Admin", "User"});
            _tracer.Trace("Returning");
            return string.Format("You entered: {0}", value);
        }


        private void CheckClaims(IEnumerable<string> requiredRoles)
        {
            _tracer.Trace("Starting");
            var identity = Thread.CurrentPrincipal.Identity as IClaimsIdentity;
            if (identity != null)
            {
                _tracer.Trace("Checking claims...");
                var nameClaim =
                    (from c in identity.Claims where c.ClaimType == ClaimTypes.Name select c.Value).SingleOrDefault();
                var roleClaims =
                    (from c in identity.Claims where c.ClaimType == ClaimTypes.Role select c.Value).ToList();
                var emailClaim =
                    (from c in identity.Claims where c.ClaimType == ClaimTypes.Email select c.Value).SingleOrDefault();
                var userDataClaim =
                    (from c in identity.Claims where c.ClaimType == ClaimTypes.UserData select c.Value).SingleOrDefault();
                if (requiredRoles.Intersect(roleClaims).Any()) return;
                _tracer.Trace("The user posseses no valid roles");
            }

            _tracer.Trace("Access denied");
            throw new SecurityException("Access is denied. Invalid Username or Password.");
        }

        public LibraryCatalog GetLibrary()
        {
            _tracer.Trace("Starting");
            CheckClaims(new[] { "Admin" });
            _tracer.Trace("Returning");
            return new LibraryCatalog { Catalog = new Hashtable { { 1, new Book() }, { 2, new Magazine() } } };
        }
    }
}